# RHCharts

[Robinhood Collection Tracker](http://206.189.206.27:8000/)

Site for tracking stats on RH collections. And me just messing around with CI and docker.

## Points of Interest and Known Issues

### Why do some stocks have unavailable data?

In order to pull foreign and OTC market data, manual scraping has to be done. Approximately 10% of the instruments on robinhood cannot be found on IEX, including high profile foreign companies such as Subaru, who's exclusion affects notable collections such as Automotives. Earlier iterations of this project have gathered data over every tradeable instrument on Robinhood via hard scraping. 

### Why is this not 1 to 1 with robinhood's percent change per time interval?

Robinhood appears to take the pre-market open price of first trading day of the time interval as the baseline. However, this app makes no distinction between trading and non-trading days and only considers open and close price. 

Timespans appear to be different. For example, 3M from 1/22/2019 is determined to be 10/24/2018 on RH, but this app subtracts three months from 1/22/2019 to get 10/22/2019.

This has drastic implications for stocks such as CRON, who's open price on the 22nd was $9.69, and who's open price on the 24th was $8.61. The data is accurate for both RH and this app, but the chosen measurement is different.

The idea solution would be to be able to batch request market data directly from RH, but this is currently not possible.

Additionally, this app does not obtain it's data directly from Robinhood, and it only updates on an hourly basis and stops updating on market close at 4 PM EST.

### Why are stocks only updated once per hour?

API rate limits and the decision to be conservative with the hardware since the droplet hosting both the DB and the server is fairly weak.

### Isn't the RH API unofficial?

Yes. There is always the possibility that Robinhood reworks their endpoints and auth in a manner that makes this tool stop working. Previous changes have not touched the structure of the JSON objects, but have only gone as far as changing endpoints or enforcing authenthication.

If Robinhood ever closes unauthenthicated access to their instrument list or tag data, this project dies.

### 1 Year data only shows since 7/20/2018

Data only goes as far back as 7/20/2018. This project is not scaled for heavy usage since it runs on a $5 DigitalOcean droplet.

Scaling this project to heavier workloads can be done by backfilling the database and moving the docker image to a stronger host, but there are no real plans to do so at the moment.