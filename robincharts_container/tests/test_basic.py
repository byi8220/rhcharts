import pytest
import os
import sys


from app import app
import db


@pytest.fixture
def client():
    client = app.test_client()
    yield client

def test_assert_app_exists():
    assert app is not None

def test_can_get_index(client):
    page = client.get('/')
    assert page is not None

def test_404(client):
    page = client.get('/doesnotexist/')

def test_db_connection():
    app.app_context().push()
    assert db.get_db() is not None