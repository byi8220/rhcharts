import os
import socket

from flask import Flask
from routes.pages import index_page
from routes.rest_routes import rest_routes
from tasks import setup_tasks, scheduler

from scraper.scrape_instruments import backfill_db, fill_collections, drop_collections, update_db

# Connect to Redis
app = Flask(__name__, static_url_path="")
app.register_blueprint(index_page)
app.register_blueprint(rest_routes)
setup_tasks(app)

if __name__ == "__main__":
    app.run()
