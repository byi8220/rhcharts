import requests
import json
import os
from datetime import datetime
import re

API_ASSET_LIMIT = 75
API_INSTRUMENTS = "https://api.robinhood.com/instruments/"
API_TAG_ROOT = "https://api.robinhood.com/midlands/tags/tag/"
API_TAG_QUOTE = "https://api.robinhood.com/marketdata/quotes/?symbols={}"

ALLOWED_RETRIES = 69

# Methods related to the RH API go here

def get_RH_historical(symbols,interval,span,headers=None):
    api_url = 'https://api.robinhood.com/marketdata/historicals/?symbols={}&interval={}&span={}'.format(symbols,interval,span)
    resp = requests.get(api_url)
    # TODO: maybe replace this with something that isn't complete garbage that locks up everything later
    retries = 0
    while resp.status_code != 200 and retries < ALLOWED_RETRIES:
        resp = requests.get(api_url,headers=headers)
        retries += 1
    if resp.status_code != 200:
        print('Request {} returned status {}'.format(api_url, str(resp.status_code)))
        return None
    return resp

def getAllTradableInstruments():
        resp = requests.get(API_INSTRUMENTS)
        retries = 0
        while resp.status_code != 200 and retries < ALLOWED_RETRIES:
            resp = requests.get(API_INSTRUMENTS)
            retries += 1
        if resp.status_code != 200:
            print('Request {} returned status {}'.format(API_INSTRUMENTS, str(resp.status_code)))
            return None
        cursor = resp.json()
        allAssets = []
        while cursor:
            for item in cursor['results']:
                if item['rhs_tradability'] == 'tradable':
                    allAssets.append(item)
            nextitem = cursor['next']
            if nextitem is not None:
                cursor = requests.get(nextitem).json()
            else:
                cursor = None
        return allAssets

def getFundamental(inst_id, headers=None):
    api_url = 'api.robinhood.com/fundamentals/{}/'.format(inst_id)
    resp = requests.get(api_url)
    retries = 0
    while resp.status_code != 200 and retries < ALLOWED_RETRIES:
        resp = requests.get(api_url,headers=headers)
        retries += 1
    if resp.status_code != 200:
        print('Request {} returned status {}'.format(api_url, str(resp.status_code)))
        return None
    return resp

def getCollection(collection, headers=None):
    api_url = API_TAG_ROOT + collection
    resp = requests.get(api_url)
    retries = 0
    while resp.status_code != 200 and retries < ALLOWED_RETRIES:
        resp = requests.get(api_url,headers=headers)
        retries += 1
    if resp.status_code != 200:
        print('Request {} returned status {}'.format(api_url, str(resp.status_code)))
        return None
    return resp

def getQuote(symbols, headers=None):
    api_url = API_TAG_QUOTE.format(symbols)
    resp = requests.get(api_url)
    retries = 0
    while resp.status_code != 200 and retries < ALLOWED_RETRIES:
        resp = requests.get(api_url,headers=headers)
        retries += 1
    if resp.status_code != 200:
        return None
    return resp

auth = None
def scrapeAuth(base='F'):
    '''Well it worked for a day didn't it?'''
    global auth
    if auth is not None:
        return auth
    resp = requests.get('https://robinhood.com/stocks/{}'.format(base))
    rt = resp.text
    authmatch = re.findall(r"access_token: '.*'",rt)
    auth_token = authmatch[0][15:-1]
    auth = auth_token
    return auth_token

def resetAuth():
    global auth
    auth = None