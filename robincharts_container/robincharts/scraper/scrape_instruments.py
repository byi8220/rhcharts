import requests
import json
import os
from datetime import datetime
from datetime import date
import sqlite3

import db
from scraper import rh_api
from scraper import alt_api

from pytz import timezone

API_ASSET_LIMIT = 75
API_INSTRUMENTS = "https://api.robinhood.com/instruments/"
API_TAG_ROOT = "https://api.robinhood.com/midlands/tags/tag/"



def backfill_db():
    '''Fill the DB with everything retrievable'''
    db.create_db()
    inst_values = []
    instruments = rh_api.getAllTradableInstruments()
    for inst in instruments:
        inst_values.append(
            (inst['id'], inst['symbol'])
        )
    db.update_db_asset(inst_values)  

    historicals = alt_api.aggregate_historical_data(instruments)
    for inst_historical in historicals:
        print(inst_historical['symbol'])
        df_hist = []
        for ih in inst_historical['chart']:
            d = {}
            d['begins_at'] = ih['date'][0:10]
            if 'open' in ih:
                d['open_price'] = ih['open']
            else:
                d['open_price'] = ih['close']
            d['close_price'] = ih['close']
            df_hist.append(d)
        db.update_db_historicals(inst_historical['symbol'], df_hist)
      
    
def drop_collections():
    db.drop_collections()

def fill_collections():
    collections = open('scraper/collections.txt', 'r').readlines()
    collection_queue = []
    for colle in collections:
        c = rh_api.getCollection(colle)
        if c is not None:
            collection_queue.append(c.json())
        else:
            print("{} is not a collection".format(colle))
    if len(collection_queue) > 0:
        db.drop_collections()
        db.rebuild_collections(collection_queue)

def get_collection_list():
    collections = open('scraper/collections.txt', 'r').readlines()
    slugs = []
    for c in collections:
        slugs.append(c.replace('\n',''))
    return slugs

def update_db():
    '''At the end of trading day commit close'''
    instruments = rh_api.getAllTradableInstruments()
    today = datetime.now(timezone('US/Eastern')).date()     
    today = str(today)
    print('==========================================', flush=True)
    print('The day is: {}. Commit today to the DB'.format(str(today)), flush=True)
    day_changes = []
    db.create_db()
    inst_values = []
    instruments = rh_api.getAllTradableInstruments()
    quotes = alt_api.get_instrument_quotes(instruments)
    for inst_quote in quotes:
        if 'close' in inst_quote['quote']:
            if 'open' in inst_quote['quote']:
                open_price = inst_quote['quote']['open']
            else:
                open_price = inst_quote['quote']['latestPrice']
            day_changes = (inst_quote['symbol'], today, open_price, inst_quote['quote']['latestPrice'])
        inst_values.append(day_changes)
    db.update_db_day(inst_values)
    now = datetime.now()
    print("{} -- Database calls returned".format(now), flush=True)
