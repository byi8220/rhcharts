import requests
import json
import os
from datetime import date, datetime, timedelta
import re
from scraper import rh_api

# Time to jank out finance data
IEX_API_MARKET = "https://api.iextrading.com/1.0/market"
IEX_SYMBOL_LIMIT = 100
RH_API_MARKET = 'https://api.robinhood.com/markets/'

# Holy fuck these are just a giant pile of dirty unmainable hacks
# 

def aggregate_historical_data(instruments, chartrange='6m'):
    '''Makes IEX api calls + scrapes for the last 300. Takes in a list of RH instruments'''
    # TODO: Eventually implement scraping non-IEX instruments if I ever bother maintaining this
    clean_hist = []
    # Get IEX supported Markets
    n = 100
    groups = [instruments[i * n:(i + 1) * n] for i in range((len(instruments) + n - 1) // n )]
    batches = []
    for g in groups:
        batches.append(list(map(lambda inst: inst['symbol'],g)))
    found = []
    notFound = []
    for batch in batches:
        batchstring = ','.join(batch)
        print(batchstring)
        req = requests.get("https://api.iextrading.com/1.0/stock/market/batch?symbols={}&types=chart&range={}"
            .format(batchstring,chartrange)).json()
        for symbol in batch:
            if symbol in req:
                found.append(symbol)
                # Historical format
                inst_hist = {
                    'symbol':symbol,
                    'chart':req[symbol]['chart']
                }
                clean_hist.append(inst_hist)
            else:
                notFound.append(symbol)

    return clean_hist

def get_instrument_quotes(instruments):
    # TODO: Eventually implement the other 300
    clean_quotes = []
    # Get IEX supported Markets
    n = 100
    groups = [instruments[i * n:(i + 1) * n] for i in range((len(instruments) + n - 1) // n )]
    batches = []
    for g in groups:
        batches.append(list(map(lambda inst: inst['symbol'],g)))
    found = []
    notFound = []
    for batch in batches:
        batchstring = ','.join(batch)
        req = requests.get("https://api.iextrading.com/1.0/stock/market/batch?symbols={}&types=quote"
            .format(batchstring)).json()
        for symbol in batch:
            if symbol in req:
                found.append(symbol)
                # Historical format
                inst_hist = {
                    'symbol':symbol,
                    'quote':req[symbol]['quote']
                }
                clean_quotes.append(inst_hist)
            else:
                notFound.append(symbol)
    return clean_quotes