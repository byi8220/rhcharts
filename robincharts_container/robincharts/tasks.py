from flask import Flask
from flask_apscheduler import APScheduler
import time
import atexit
import os
from datetime import datetime
from pytz import utc

from scraper.scrape_instruments import backfill_db, fill_collections, drop_collections, update_db
from scraper.rh_api import resetAuth

scheduler = APScheduler()
def setup_tasks(app):
    if os.getenv('REBUILD_DATABASE') == 'REBUILD':
        rebuild_db()
    scheduler.init_app(app)
    scheduler.add_job("update_day_task",update_day_task, trigger='cron', day_of_week='mon-fri', hour='14-22', timezone=utc,
        next_run_time=datetime.now())
    scheduler.add_job("update_collections_task",update_collections_task, trigger='cron', hour=13, timezone=utc,
        next_run_time=datetime.now())
    scheduler.start()
    atexit.register(lambda: scheduler.shutdown())

def update_collections_task():
    print(time.strftime("%A, %d. %B %Y %I:%M:%S %p"), flush=True)
    print("Rebuilding Collections",flush=True)
    fill_collections()

def update_day_task():
    print(time.strftime("%A, %d. %B %Y %I:%M:%S %p"))
    update_db()

def rebuild_db():
    print('====================')
    print('Backfill 6 months in progress')
    backfill_db()
    print('Collection in progress')
    fill_collections()
