import sqlite3
from flask import g
import os

# Lol I could use an ORM but I was really fucking lazy and I can't write database code for shit

database = None
def get_db():
    global database
    if database is None:
        database_path = os.getenv('RH_DATABASE')
        if database_path is None or os.getenv('DATABASE_MODE') == "TEST":
            database_path = 'testing_database.db'
        database = sqlite3.connect(database_path, check_same_thread=False)
    return database

def create_db():
    conn = get_db()
    conn.execute('''CREATE TABLE IF NOT EXISTS assets
    (id text, symbol text, PRIMARY KEY(id))''')
    conn.execute('''CREATE TABLE IF NOT EXISTS historicals
    (symbol text, date text, open_price, close_price, PRIMARY KEY(symbol, date))''')
    conn.execute('''CREATE TABLE IF NOT EXISTS collections
    (slug text, name text, inst_id text, inst_symbol text)''')
    conn.commit()

def update_collection(collection, commit=False):
    '''Assuming the instrument table is already filled'''
    conn = get_db()
    cur = conn.cursor()
    colle_values = []
    inst_values = []
    for inst in collection['instruments']:
        inst_id = inst.replace('https://api.robinhood.com/instruments/', '')[:-1]
        cur.execute('''SELECT id, symbol FROM assets WHERE id=?''', (inst_id,))
        inst_values.append(cur.fetchone())
    for iv in inst_values:
        if iv is not None:
            colle_values.append((collection['slug'],collection['name'], iv[0], iv[1]))
    cur.executemany("INSERT OR REPLACE INTO collections values (?,?,?,?)", colle_values)
    if commit:
        conn.commit()

def drop_collections(commit=False):
    conn = get_db()
    cur = conn.cursor()
    conn.execute('''DROP TABLE IF EXISTS collections''')
    conn.execute('''CREATE TABLE IF NOT EXISTS collections
    (slug text, name text, inst_id text, inst_symbol text)''')
    if commit:
        conn.commit()

def rebuild_collections(cq):
    conn = get_db()
    cur = conn.cursor()
    drop_collections(commit = False)
    for c in cq:
        update_collection(c, commit=False)
    conn.commit()
    

def update_db_asset(asset_values):
    conn = get_db()
    conn.cursor().executemany("INSERT OR REPLACE INTO assets VALUES (?,?)", asset_values)
    conn.commit()

def stress():
    symbols = get_collection_members('technology')
    print(symbols)
    print(len(symbols))
    sd = get_collection_historicals('technology')
    print(len(sd))

def update_db_historicals(symbol, histdata):
    conn = get_db()
    cur = conn.cursor()
    hist_values = []
    for day in histdata:
        hist_values.append((symbol, day['begins_at'],day['open_price'],day['close_price']))
    cur.executemany("INSERT OR REPLACE INTO historicals values (?,?,?,?)", hist_values)
    conn.commit()

def update_db_day(day_changes):
    conn = get_db()
    cur = conn.cursor()
    cur.executemany("INSERT OR REPLACE INTO historicals values (?,?,?,?)", day_changes)
    conn.commit()

def get_collection_members(collection_slug):
    conn = get_db()
    cur = conn.cursor()
    cur.execute('''SELECT symbol FROM assets,collections WHERE assets.symbol = collections.inst_symbol AND collections.slug=?'''
        ,(collection_slug,))
    rows = cur.fetchall()
    return rows

def get_collection_historicals(collection_slug):
    conn = get_db()
    cur = conn.cursor()
    cur.execute('''SELECT symbol,date,open_price, close_price FROM historicals,collections 
        WHERE historicals.symbol=collections.inst_symbol AND collections.slug=?''',(collection_slug,))
    results = cur.fetchall()
    return results