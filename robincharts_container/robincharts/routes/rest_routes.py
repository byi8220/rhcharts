from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound
import db
from scraper.scrape_instruments import get_collection_list
import json

rest_routes = Blueprint('rest_routes', __name__,
                        template_folder='templates')

@rest_routes.route('/api/collection_members/<slug>/')
def collection_members(slug):
    symbols = db.get_collection_members(slug)
    if len(symbols) == 0:
        return "Collection '{}' is not found".format(slug)
    arr = [s[0] for s in symbols]
    return json.dumps(arr)

@rest_routes.route('/api/collection_historicals/<slug>/')
def collection_historicals(slug):
    historicals = db.get_collection_historicals(slug)
    hist_by_symbol = {}
    if len(historicals) == 0:
        return "Collection '{}' is not found".format(slug)
    for symbol, date, open_price, close_price in historicals:
        if symbol not in hist_by_symbol:
            hist_by_symbol[symbol] = {}
        hist_by_symbol[symbol][date] = {}    
        hist_by_symbol[symbol][date]['close'] = close_price
        hist_by_symbol[symbol][date]['open'] = open_price
    return json.dumps(hist_by_symbol)


@rest_routes.route('/api/collections')
def collections():
    return json.dumps(get_collection_list())