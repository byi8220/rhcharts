from flask import Blueprint, render_template, abort, current_app, send_from_directory
from jinja2 import TemplateNotFound
import requests

from scraper.scrape_instruments import backfill_db, fill_collections, drop_collections, update_db

index_page = Blueprint('index_page', __name__,
                        template_folder='templates')

@index_page.route('/')
def index():
    try:
        return current_app.send_static_file('index.html')
    except TemplateNotFound:
        abort(404)
