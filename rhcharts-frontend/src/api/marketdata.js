import axios from 'axios'

export default {
    getCollections(){
        return axios.get('/api/collections', {headers:{'Content-Type': 'application/json'}})
    },

    getCollectionMembers(slug){
        return axios.get('/api/collection_members/' + slug, {headers:{'Content-Type': 'application/json'}});
    },

    getCollectionHistoricals(slug){
        return axios.get('/api/collection_historicals/' + slug, {headers:{'Content-Type': 'application/json'}});
    }
}