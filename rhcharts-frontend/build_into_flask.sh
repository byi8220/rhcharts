npm run build
rm -r ../robincharts_container/robincharts/static
mkdir ../robincharts_container/robincharts/static\

cp -r ./dist/css ../robincharts_container/robincharts/static/
cp -r ./dist/js ../robincharts_container/robincharts/static/
cp ./dist/index.html ../robincharts_container/robincharts/static/index.html